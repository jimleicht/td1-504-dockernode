Observations : 

Il est possible de déployer une application NodeJS via Gitlab Pages.
Le projet est ensuite accessible via https://username.gitlab.io/nom-du-projet
Pour cela, on peut utiliser un fichier .gitlab-ci.yml.

L'ensemble des fichiers de notre application est stocké dans un dossier public à la racine du projet.
Dans le gitlab-ci.yml, on peut préciser une image Docker qui va être exécuté (ici, node:lts)
Sous la mention before_scripts, on précise les commandes à lancer avant le lancement de l'application (installation des dépendances)
Sous la mention scripts, les scripts à lancer pour démarrer correctement l'application.

Je n'ai pas réussi à comprendre comment compiler correctement le scss pour finir le déploiement.